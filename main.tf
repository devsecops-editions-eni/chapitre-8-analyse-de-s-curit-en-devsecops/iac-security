resource "google_compute_network" "vpc_network" {
  name                    = "my-custom-mode-network"
  auto_create_subnetworks = true
  mtu                     = 1460
}

resource "google_compute_instance" "nginx" {
  name         = "nginx"
  machine_type = "f1-micro"
  zone         = "europe-west1-b"
  tags         = ["ssh"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  # Install NGINX
  metadata_startup_script = "sudo apt update; sudo apt install -y nginx"

  network_interface {
    network = google_compute_network.vpc_network.id
  }
}

resource "google_compute_instance" "mysql" {
  name         = "mysql"
  machine_type = "f1-micro"
  zone         = "europe-west1-b"
  tags         = ["ssh"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  # Install MYSQL
  metadata_startup_script = "sudo apt update; sudo apt install wget; wget https://dev.mysql.com/get/mysql-apt-config_0.8.22-1_all.deb; sudo apt install ./mysql-apt-config_0.8.22-1_all.deb; sudo apt update; sudo apt install mysql-server"

  network_interface {
    network = google_compute_network.vpc_network.id
  }
}

resource "google_compute_instance" "backend" {
  name         = "backend"
  machine_type = "f1-micro"
  zone         = "europe-west1-b"
  tags         = ["ssh"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  # Install Python and some packages
  metadata_startup_script = "sudo apt update; sudo apt install -yq build-essential python3-pip rsync"

  network_interface {
    network = google_compute_network.vpc_network.id
  }
}
